!<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

    </head>
    <body>
       <div id="app">
           <h2>Tambah Menu</h2>
           <input type="text" v-model="nama" placeholder="masukkan nama menu..">
           <input type="text" v-model="harga" placeholder="masukkan harga menu..">
           <button @click="tambahMenu">Kirim!</button>
           <h3>Menu Warteg Bu Yanti</h3>
           <div v-for="(menu, index) in menus">
               <div v-if="menu.status == 0">
                <p>Nama: @{{menu.nama}}</p>
                <p> Harga: @{{menu.harga}}</p>
                   <p> Masih ada</p>
               </div>
               <div v-if="menu.status == 1">
                   <p style="text-decoration: line-through;">Nama: @{{menu.nama}}</p>
                   <p style="text-decoration: line-through;"> Harga: @{{menu.harga}}</p>
                   <p> Sudah habis</p>
               </div>
               <button @click="ubahStatus(menu, menu.id)">Ubah Status</button>
               <button @click="hapusMenu(index, menu.id)">Hapus Menu</button><br>
               =================================================== 
           </div>
       </div>
        <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>

        <script>
            new Vue({
                el: "#app",
                data:{
                    nama: "",
                    harga: "",
                    menus: [],
                },
                methods: {
                    ubahStatus: function(menu, id){
                        this.$http.post('/api/menu/change-status/' + id, {nama: this.nama, harga: this.harga}).then(response => {
                            menu.status = !menu.status
                        });
                    },
                    tambahMenu: function(){
                        this.$http.post('/api/menu', {nama: this.nama, harga: this.harga}).then(response => {
                             this.menus.unshift({nama: this.nama, harga: this.harga, status: 0});
                        });
                    },
                    hapusMenu: function(index, id){
                        this.$http.post('/api/menu/delete/' + id).then(response => {
                            this.menus.splice(index, 1);                           
                        });
                    },
                },
                mounted:function(){
                    this.$http.get('/api/menu').then(response => {
                        this.menus = response.body.data;
                    });
                }
            });
        </script>
    </body>
</html>
