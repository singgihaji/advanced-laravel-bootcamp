<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/menu', 'MenuController@index');
Route::post('/menu', 'MenuController@store');
Route::post('/menu/change-status/{id}', 'MenuController@updateStatus');
Route::post('/menu/delete/{id}', 'MenuController@destroy');
