<?php


Route::post('register', 'Auth\RegisterController');
Route::post('login', 'Auth\LoginController');
Route::post('logout', 'Auth\LogoutController');
Route::get('user', 'UserController');

Route::middleware('auth:api')->group(function () {
    Route::post('create-new-book', 'BookController@store');
    Route::patch('update-book/{book}', 'BookController@update');
    Route::delete('delete-book/{book}', 'BookController@destroy');

    Route::post('create-new-pinjam', 'PinjamController@store');
    Route::patch('update-pinjam/{pinjam}', 'PinjamController@update');
    Route::delete('delete-pinjam/{pinjam}', 'PinjamController@destroy');
    Route::patch('update-pinjam-admin/{pinjam}', 'PinjamController@updateByAdmin')->middleware('admin');
});

Route::get('book/{book}', 'BookController@show');
