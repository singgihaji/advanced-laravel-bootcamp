<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $primaryKey = 'kd_buku';
    protected $fillable = [
        'judul', 'pengarang', 'tahun_terbit'
    ];

    public function getRouteKeyName()
    {
        return 'kd_buku';
    }

    public function user()
    {
        $this->belongsTo(User::class);
    }

    public function pinjam()
    {
        $this->belongsTo(Pinjam::class);
    }
}
