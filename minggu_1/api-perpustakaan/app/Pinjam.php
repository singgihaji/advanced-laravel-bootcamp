<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pinjam extends Model
{
    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function book()
    {
        return $this->belongsTo(Book::class);
    }
}
