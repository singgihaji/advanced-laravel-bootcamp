<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {

        User::create([
            'username' => request('username'),
            'password' => bcrypt(request('password')),
            'nama' => request('nama'),
            'nim' => request('nim'),
            'jurusan' => request('jurusan'),
            'fakultas' => request('fakultas'),
            'no_hp' => request('no_hp'),
            'no_wa' => request('no_wa'),
            'roles' => request('roles')
        ]);

        return response('thanks for registered');
    }
}
