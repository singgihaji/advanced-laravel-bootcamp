<?php

namespace App\Http\Controllers;

use App\Book;
use App\Http\Requests\PinjamRequest;
use App\Pinjam;
use App\User;
use Illuminate\Http\Request;

class PinjamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pinjam = Pinjam::get();
        return response()->json(['status' => 'success', 'data' => $pinjam]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::findOrFail(auth()->user()->id);
        $pinjam = Pinjam::create([
            'user_id' => auth()->user()->id,
            'kd_buku' => request('kd_buku'),
            'tanggal_pinjam' => request('tanggal_pinjam'),
            'batas_akhir_pinjam' => request('batas_akhir_pinjam'),
            'tanggal_kembali' => request('tanggal_kembali')
        ]);
        $book = Book::findOrFail($pinjam->kd_buku);
        return response()->json(['status' => 'success', 'data' => $pinjam, 'user' => $user, 'book' => $book]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pinjam = Pinjam::findOrFail($id);

        return response()->json(['status' => 'success', 'data' => $pinjam]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PinjamRequest $request, Pinjam $pinjam)
    {

        $pinjam->update([
            'kd_buku' => request('kd_buku'),
            'tanggal_pinjam' => request('tanggal_pinjam'),
            'batas_akhir_pinjam' => request('batas_akhir_pinjam')
        ]);
        return response()->json(['data' => $pinjam]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateByAdmin(Request $request, Pinjam $pinjam)
    {
        $request->validate([
            'kd_buku' => 'sometimes',
            'tanggal_pinjam' => 'sometimes',
            'batas_akhir_pinjam' => 'sometimes',
            'tanggal_kembali' => 'sometimes',
            'status_ontime' => 'sometimes'

        ]);

        $pinjam->update([
            'kd_buku' => request('kd_buku'),
            'tanggal_pinjam' => request('tanggal_pinjam'),
            'batas_akhir_pinjam' => request('batas_akhir_pinjam'),
            'tanggal_kembali' => request('tanggal_kembali'),
            'status_ontime' => request('status_ontime')
        ]);

        return response()->json(['status' => 'success', 'data' => $pinjam]);
    }
}
