<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => ['required', 'min:3', 'max:50'],
            'password' => ['required'],
            'nama' => ['required', 'min:3', 'max:50'],
            'nim' => ['required', 'max:12'],
            'fakultas' => ['required', 'max:50'],
            'jurusan' => ['required', 'max:50'],
            'no_hp' => ['required', 'max:15'],
            'no_wa' => ['required', 'max:15'],
            'roles' => ['required', 'in:admin,mahasiswa']
        ];
    }
}
