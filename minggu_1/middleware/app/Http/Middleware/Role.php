<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Request;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $my_role = Auth::user()->role;
        $my_url = Request::segment(1);

        if ($my_role == 'SuperAdmin') {
            return $next($request);
        } elseif ($my_role == 'Admin') {
            if ($my_url == 'route-2' || $my_url == 'route-3') {
                return $next($request);
            } else {
                return abort(403);
            }
        } elseif ($my_role == 'Guest') {
            if ($my_url == 'route-3') {
                return $next($request);
            } else {
                return abort(403);
            }
        }
    }
}
