@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                       
                    @endif
                    @if(Auth::user()->role == 'Guest')
                    My Role is {{Auth::user()->role}}<br>
                    I can only access Route-3  <br>
                    @endif
                    @if(Auth::user()->role == 'Admin')
                    My Role is {{Auth::user()->role}}<br>
                    I can access Route-2 and Route-3  <br>
                    @endif
                    @if(Auth::user()->role == 'SuperAdmin')
                    My Role is {{Auth::user()->role}}<br>
                    I can access all Routes  <br>
                    @endif
                    <a href={{route('route-1')}}>Route-1</a><br>
                    <a href={{route('route-2')}}>Route-2</a><br>
                    <a href={{route('route-3')}}>Route-3</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
