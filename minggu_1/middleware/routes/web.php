<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/home', function () {
//     return Request::segment(1);
// });
Route::middleware('role')->group(function () {
    Route::get('/route-1', function () {
        return "/route-1";
    })->name('route-1');

    Route::get('/route-2', function () {
        return "/route-2";
    })->name('route-2');

    Route::get('/route-3', function () {
        return "/route-3";
    })->name('route-3');
});
