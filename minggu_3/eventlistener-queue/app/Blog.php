<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function user()
    {
        $this->belongsTo(User::class);
    }
}
