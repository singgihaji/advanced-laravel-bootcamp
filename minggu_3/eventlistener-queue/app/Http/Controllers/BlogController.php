<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\Events\BlogCreatedEvent;
use App\Events\BlogPublishedEvent;
use App\Mail\BlogPublishedMail;
use App\User;


class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return reponse()->json(['data' => Blog::get()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $blog = Blog::create([
            'title' => request('title'),
            'body' => request('body'),
            'user_id' => auth()->user()->id
        ]);



        $user = User::findOrFail(auth()->user()->id);

        event(new BlogCreatedEvent($user, $blog->title));

        return response()->json(['status' => 'success', 'data' => $blog]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        $blog->update([
            'publish_status' => 1
        ]);

        $user = User::findOrFail(auth()->user()->id);
        event(new BlogPublishedEvent($user, $blog->title));
        return response()->json(['message' => 'Berhasil publish!', 'data' => $blog]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
