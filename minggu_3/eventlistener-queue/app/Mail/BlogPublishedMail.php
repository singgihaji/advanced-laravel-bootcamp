<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;

class BlogPublishedMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $title;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $title)
    {
        $this->user = $user;
        $this->title = $title;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('example@example.com')
            ->view('send_email_blog_published')
            ->with(['name' => $this->user->name, 'title' => $this->title]);
    }
}
