<?php

use Illuminate\Http\Request;

Route::post('register', 'RegisterController');
Route::post('login', 'LoginController');
Route::get('user', 'UserController');
Route::post('create-new-blog', 'BlogController@store');

Route::middleware('auth:api')->group(function () {
    Route::patch('update-blog/{blog}', 'BlogController@update');
    Route::delete('delete-blog/{blog}', 'BlogController@destroy');
});
